package com.example.practicaconcomposo

import android.annotation.SuppressLint
import android.content.ClipData.Item
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import com.example.practicaconcomposo.ui.theme.PracticaConComposoTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PracticaConComposoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background
                ) {
                    MyApp()
                }
            }
        }
    }
}


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MyApp() {
    Scaffold(content = {
        Greeting(name = "Android")
    })
}


@Composable
fun Greeting(name: String) {
    Column(
        modifier = Modifier
            .background(Color.Gray)
            .fillMaxSize()
            .padding(16.dp, 16.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {

        Image(
            painter = painterResource(id = R.drawable.milanga),
            contentDescription = "Photo Of Details",
            modifier = Modifier
                .padding(start = 16.dp, top = 16.dp, end = 16.dp)
                .size(295.dp)
                .clip(RectangleShape)
        )

        Text(
            text = "MILANESA A LA RIOJANA",
            color = Color.Black,
            modifier = Modifier.padding(start = 16.dp, top = 24.dp, end = 16.dp, bottom = 16.dp),
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Black,
            fontSize = 35.sp
        )
        Text(
            text = "La milanesa a la napolitana es un plato típico de la gastronomía rioplatense propia de Argentina y Uruguay que tiene su origen en la influencia de la inmigración italiana. Consiste en una milanesa, habitualmente de carne vacuna, llevada al horno para ser recubierta como una pizza, con rodajas de tomate y queso mozzarella, añadiendo diversos ingredientes (como jamón o cebolla). Habitualmente se sirve con una guarnición de papas fritas",
            color = Color.DarkGray,
            modifier = Modifier.padding(start = 16.dp, top = 24.dp, end = 16.dp, bottom = 16.dp),
            textAlign = TextAlign.Justify
        )
        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.End
        ) {

            Button(
                onClick = { /*TODO*/ },
                border = BorderStroke(2.dp, Color.Red),
                modifier = Modifier
                    .padding(end = 8.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Red)
            ) {
                Text(text = "No,\ngracias\nestoy a dieta", textAlign = TextAlign.Center)
            }
            Button(
                onClick = { /*TODO*/ },
                border = BorderStroke(2.dp, Color.Green),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Green)
            ) {
                Text(text = "Damela\nintravenosa",textAlign = TextAlign.Center)
            }
        }
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PracticaConComposoTheme {
        Greeting("Android")
    }
}